package com.example;

public class Circle implements Shape{
    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        return (this.radius * Math.PI * Math.PI);
    }

    public double getPerimeter(){
        return 2 * Math.PI * this.radius;
    }
}
