package com.example;

interface Shape{
    public double getArea();
    public double getPerimeter();
}