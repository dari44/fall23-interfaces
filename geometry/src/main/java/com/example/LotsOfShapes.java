package com.example;

/**
 * Hello world!
 *
 */
public class LotsOfShapes 
{
    public static void main( String[] args )
    {
        Shape[] shapes = new Shape[5];
        shapes[0] = new Circle(5);
        shapes[1] = new Circle(4);
        shapes[2] = new Rectangle(2, 3);
        shapes[3] = new Rectangle(4,6);
        shapes[4] = new Square(3);

        for (Shape s : shapes){
            System.out.println(s.getArea() + ", " + s.getPerimeter());
        }
     }
}
